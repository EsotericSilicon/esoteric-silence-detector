# Esoteric Silence Detector
This is a Python application that helps live streamers monitor their microphone. It provides visual feedback about the streamer's mute state and the amount of time spent talking, which can help improve viewer retention and prevent accidental mutes.

## Features
Monitors the selected audio input device in real-time.

Provides visual feedback about the streamer's mute state and the amount of time spent talking.

Changes color to indicate the status of the audio: green when the streamer is talking, then yellow, red, and red flashing the longer the streamer is quiet. Flashes yellow when streamer is too loud. 

Displays the percentage of time the streamer has spent talking.

Prevents the system from going to sleep while the application is running.

Saves the settings to a file and loads them when the application starts.

Supports Windows, macOS, and Linux.

## Required Libraries
SoundDevice 

Numpy 

## Windows EXE

There is a precompiled exe file in the Dist folder for Windows users. Compiled code can sometimes be falsely identified as a virus, however you can compile it yourself using PyInstaller: 

pyinstaller --hidden-import SoundDevice --hidden-import numpy --onefile --icon .\mic_128.ico  --windowed  --name EsotericSilenceDetector --add-data "mic_128.ico:." --add-data "mic_128.png:." SilenceAlarm.py

## Usage

1. Make sure you have Numpy and SoundDevice installed, then run the script using Python 3. If you don't have Python installed, you can install Thonny, which includes Python 3. Use Tools > Manage Packages to install Numpy and SoundDevice, then open SilenceAlarm.py and click the green run button at the top. 
2. Select the audio input device from the dropdown menu at the top of the window.
3. Adjust the noise floor and noise ceiling levels using the scales.
4. Set the green, yellow, and red alert levels by entering the values in the corresponding entry fields.

The colored box at the center of the window will change color based on the current audio level and the alert levels. The box will be green when the streamer is talking, yellow when the streamer is quiet, and red when the audio is too loud. Double-click to hide the options, so the colored box takes up more of the window. 

😶 shows the amount of time the streamer has been quiet. It displays a "quiet" face emoji when the streamer is quiet, a "smiling talking" face emoji when the streamer is talking, and a "yelling" face emoji when the audio is too loud.

🗣️ shows the percentage of time the streamer has spent talking. This can help the streamer track how much time they are speaking during each stream and compare it to viewer retention.

Click the ⏱️(Reset) button to reset the silence time and green time counters.

The settings will be saved to a file when the application is closed and loaded when the application starts.