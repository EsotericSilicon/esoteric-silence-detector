import sounddevice as sd
import numpy as np
import tkinter as tk
from tkinter import ttk
import json
import ctypes
import platform
import os
import subprocess


def resource_path(relative_path):
    try:
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")

    return os.path.join(base_path, relative_path)
    
def prevent_sleep():
    if platform.system() == 'Windows':
        ES_CONTINUOUS = 0x80000000
        ES_SYSTEM_REQUIRED = 0x00000001
        ES_DISPLAY_REQUIRED = 0x00000002
        ctypes.windll.kernel32.SetThreadExecutionState(
            ES_CONTINUOUS | ES_SYSTEM_REQUIRED | ES_DISPLAY_REQUIRED)
    elif platform.system() == 'Darwin':
        subprocess.call("caffeinate -u -t 1", shell=True)
    elif platform.system() == 'Linux':
        subprocess.call("xset s off", shell=True)
# Call the function at the start of your script
prevent_sleep()

# Load settings from file
try:
    with open('settings.json', 'r') as f:
        settings = json.load(f)
except FileNotFoundError:
    settings = {}


# Create a Tkinter window
window = tk.Tk()
window.geometry('350x180')
window.title('Esoteric Silence Detector')

if platform.system() == 'Windows':
    window.iconbitmap(resource_path("mic_128.ico"))
elif platform.system() == 'Linux':
    icon = tk.PhotoImage(file=resource_path("mic_128.png"))
    window.iconphoto(True, icon)
#elif platform.system() == 'Darwin':

window.minsize(350,160)

# Global variables
audio_level = 0
silence_time = 0
green_time = 0
total_time = 0
yellow_flash_timer = 0
expanded = False
green_time_label_top = None


# Query Devices
devices = sd.query_devices()
device_names = [device['name'] for device in devices if device['max_input_channels'] > 0]

# Global variables for all the settings
mic = tk.StringVar(value=settings.get('mic', device_names[0]))
noise_floor = tk.DoubleVar(value=settings.get('noise_floor', 15))
noise_ceiling = tk.DoubleVar(value=settings.get('noise_ceiling', 85))
green_alert_level = tk.StringVar(value=settings.get('green_alert_level', "10"))
yellow_alert_level = tk.StringVar(value=settings.get('yellow_alert_level', "20"))
red_alert_level = tk.StringVar(value=settings.get('red_alert_level', "30"))


# Function to process audio data
def audio_callback(indata, frames, time, status):
    global audio_level
    volume_norm = np.linalg.norm(indata[:, 0]) * 10  # Process only the first channel
    audio_level = int(volume_norm)

# Create a Frame for the microphone selection and monitoring button
mic_frame = tk.Frame(window)
mic_frame.pack(fill='x')

mic_emoji = tk.Label(mic_frame, text="🎤", font=("Arial", 11))
mic_emoji.pack(side='left')

# Create a Combobox for microphone selection
mic_selection = ttk.Combobox(mic_frame, width=50, textvariable=mic)  # Use textvariable option
mic_selection['values'] = device_names
try:
    mic_selection.current(device_names.index(mic.get()))
except ValueError:
    mic_selection.current(0)
mic_selection.pack(side='left')

# Function to start monitoring
def start_monitoring(event=None):  # Add event parameter for binding
    device_name = mic.get()
    device_id = device_names.index(device_name)
    channels = devices[device_id]['max_input_channels']
    global stream
    if 'stream' in globals():  # Stop the existing stream if it exists
        stream.stop()
    stream = sd.InputStream(device=device_id, channels=channels, callback=audio_callback)
    stream.start()
    # Save microphone setting if it has changed
    if settings.get('mic') != device_name:
        settings['mic'] = device_name
        with open('settings.json', 'w') as f:
            json.dump(settings, f)
            
# Bind the Combobox to start monitoring when a selection is made
mic_selection.bind('<<ComboboxSelected>>', start_monitoring)

# Create a Button to start monitoring
#start_button = tk.Button(mic_frame, text="Start Monitoring", command=start_monitoring)
#start_button.pack(side='left')

# Create a Canvas for color changing rectangle
canvas = tk.Canvas(window, width=1, height=1)
canvas.pack(fill='both', expand=True)

# Create a Frame for the silence time and alert levels
alert_frame = tk.Frame(window)
alert_frame.pack(fill='x')

# Create a Label for silence time
silence_time_label = tk.Label(alert_frame, text="😶 0", font=("Arial", 20))
silence_time_label.pack(side='left')


# Create a Label for green time percentage
green_time_label = tk.Label(alert_frame, text="🗣 0%", font=("Arial", 20))
green_time_label.pack(side='right')

# Function to reset counter
def reset_counter():
    global green_time, total_time, silence_time
    green_time = 0
    total_time = 0
    silence_time = 0
    
# Create a Button to reset counter
reset_button = tk.Button(alert_frame, text="⏱️", command=reset_counter, font=("Arial", 14))
reset_button.pack(side='left')

    
# Create a Tcl wrapper for the validation function
def validate_input(P):
    if P == '':
        return True
    try:
        value = int(P)
        if value >= 1:
            return True
        else:
            return False
    except ValueError:
        return False

vcmd = (window.register(validate_input), '%P')



# Function to save alert level settings
def save_alert_level(event=None):
    # Save alert level settings if they have changed
    green_level = green_alert_level.get()
    yellow_level = yellow_alert_level.get()
    red_level = red_alert_level.get()
    if (settings.get('green_alert_level') != green_alert_level.get() or
        settings.get('yellow_alert_level') != yellow_alert_level.get() or
        settings.get('red_alert_level') != red_alert_level.get() or
        settings.get('noise_floor') != noise_floor_scale.get() or
        settings.get('noise_ceiling') != noise_ceiling_scale.get()
        
        ):
        settings['green_alert_level'] = green_alert_level.get()
        settings['yellow_alert_level'] = yellow_alert_level.get()
        settings['red_alert_level'] = red_alert_level.get()
        
        settings['noise_floor'] = noise_floor_scale.get()
        settings['noise_ceiling'] = noise_ceiling_scale.get()
        with open('settings.json', 'w') as f:
            json.dump(settings, f)


# Create Entry widgets for alert levels
green_alert_level_entry = tk.Entry(alert_frame, width=4, validate='key', validatecommand=vcmd, textvariable=green_alert_level)  # Use textvariable option
green_alert_level_entry.pack(side='left')

yellow_alert_level_entry = tk.Entry(alert_frame, width=4, validate='key', validatecommand=vcmd, textvariable=yellow_alert_level)  # Use textvariable option
yellow_alert_level_entry.pack(side='left')

red_alert_level_entry = tk.Entry(alert_frame, width=4, validate='key', validatecommand=vcmd, textvariable=red_alert_level)  # Use textvariable option
red_alert_level_entry.pack(side='left')

# Create a Frame for the noise floor scale
scale_frame = tk.Frame(window)
scale_frame.pack(fill='x')

# Create a Scale for noise floor
noise_floor_scale = ttk.Scale(scale_frame, from_=0, to=100, variable=noise_floor)  # Use variable option
noise_floor_scale.set(settings.get('noise_floor', 15))  # Start at 15% or the saved value
noise_floor_scale.pack(fill='x')


# Create a Frame for the audio level bar
audio_frame = tk.Frame(window)
audio_frame.pack(fill='x')

# Create a Progressbar for audio level
audio_level_bar = ttk.Progressbar(audio_frame, mode='determinate')
audio_level_bar.pack( fill='x')

# Create a Frame for the noise ceiling scale
scale_frame = tk.Frame(window)
scale_frame.pack(fill='x')


# Create a Scale for noise ceiling
noise_ceiling_scale = ttk.Scale(scale_frame, from_=0, to=100, variable=noise_ceiling)  # Use variable option
noise_ceiling_scale.set(85)  # Start at 85%
noise_ceiling_scale.pack(fill='x')



# Function to update GUI
def update_gui():
    global audio_level, silence_time, green_time, total_time, yellow_flash_timer
    if not window.winfo_exists():  # Stop the loop if the window is destroyed
        return
    noise_floor = noise_floor_scale.get()
    noise_ceiling = noise_ceiling_scale.get()
    try:
        green_level = float(green_alert_level.get())
    except ValueError:
        green_level = 0.0  # Default value
    try:
        yellow_level = float(yellow_alert_level.get())
    except ValueError:
        yellow_level = 0.0  # Default value
    try:
        red_level = float(red_alert_level.get())
    except ValueError:
        red_level = 0.0  # Default value

    if audio_level < noise_floor:
        silence_time += 0.1
        if silence_time >= red_level:
            if int(silence_time/1.5) % 2 == 0:
                silence_time_label['text'] = f"😪 {silence_time:.1f}"
            else:
                silence_time_label['text'] = f"😴 {silence_time:.1f}"
                
        elif silence_time >= yellow_level:
            silence_time_label['text'] = f"🥱 {silence_time:.1f}"
        else:
            silence_time_label['text'] = f"😶 {silence_time:.1f}"
    elif audio_level > noise_ceiling:
        silence_time = 0
        silence_time_label['text'] = f"😆 !!!"
    else:
        silence_time = 0
        silence_time_label['text'] = f"😄 {silence_time:.1f}"
    
    if audio_level > noise_ceiling and yellow_flash_timer < 10:
        yellow_flash_timer = 20  # Reset the timer to 2 seconds
    elif yellow_flash_timer > 0:
        yellow_flash_timer -= 1  # Decrement the timer
        if int(yellow_flash_timer / 5) % 2 == 0:  # Flash every 0.5 seconds
            canvas.config(bg='yellow')
        else:
            canvas.config(bg='black')
    elif silence_time < green_level:
        canvas.config(bg='green')
        green_time += 0.1
    elif silence_time < yellow_level:
        canvas.config(bg='yellow')
    elif silence_time < red_level:
        canvas.config(bg='red')
    else:
        if int(silence_time%9) % 2 == 0:
            canvas.config(bg='red')
        else:
            canvas.config(bg='black')
    total_time += 0.1
    green_time_label['text'] = f"🗣 {green_time / total_time * 100:.1f}%"
            
    audio_level_bar['value'] = audio_level
            
    window.after(100, update_gui)  # Update every 100ms

# Function to update the Toplevel window
def update_gui_top():
    global audio_level
    if not top.winfo_exists() or not expanded: 
        return
    
    audio_level_bar_top['value'] = audio_level
    canvas_top.config(bg=canvas.cget('bg'))  # Update the background color
    green_time_label_top['text'] = green_time_label['text']  # Update the green time label
    silence_time_label_top['text'] = silence_time_label['text']  # Update the silence time label
    top.after(100, update_gui_top)  # Update every 100ms
    
# Function to save settings
def save_settings():
    settings['mic'] = mic.get()
    settings['noise_floor'] = noise_floor.get()
    settings['noise_ceiling'] = noise_ceiling.get()
    settings['green_alert_level'] = green_alert_level.get()
    settings['yellow_alert_level'] = yellow_alert_level.get()
    settings['red_alert_level'] = red_alert_level.get()
    with open('settings.json', 'w') as f:
        json.dump(settings, f)

# Function to expand or shrink the colored box
def toggle_expand(event):
    global expanded, top, green_time_label_top, canvas_top, silence_time_label_top, audio_level_bar_top
    if expanded:
        # Shrink the colored box
        top.destroy()
        top = None
        green_time_label_top = None
        window.deiconify()  # Show the main window
        expanded = False
    else:
        # Expand the colored box
        top = tk.Toplevel(window)
        # Bind the function to the window's <Destroy> event
        #top.bind('<Destroy>', window.destroy)
        top.protocol('WM_DELETE_WINDOW', window.destroy)  # Use protocol method to bind to the close button event
        top.minsize(75,26)
        top.geometry(f"{window.winfo_width()}x{window.winfo_height()}+{window.winfo_x()}+{window.winfo_y()}")
        canvas_top = tk.Canvas(top, bg=canvas.cget('bg'), width=top.winfo_width(), height=top.winfo_height())
        canvas_top.pack(fill='both', expand=True)
        audio_frame_top = tk.Frame(top)  
        audio_frame_top.pack(fill='x')
        audio_level_bar_top = ttk.Progressbar(audio_frame_top, mode='determinate')
        audio_level_bar_top.pack(fill='x')
        alert_frame_top = tk.Frame(top)
        alert_frame_top.pack(fill='x')
        reset_button_top = tk.Button(alert_frame_top, text="⏱️", command=reset_counter, font=("Arial", 14))
        reset_button_top.pack(side='left')
        silence_time_label_top = tk.Label(alert_frame_top, text="😶 0%", bg='white', font=("Arial", 20))
        silence_time_label_top.pack(side='left')
        green_time_label_top = tk.Label(alert_frame_top, text="🗣 0%", bg='white', font=("Arial", 20))
        green_time_label_top.pack(side='right')
        canvas_top.bind('<Double-Button-1>', toggle_expand)
        expanded = True
        window.iconify()  # Hide the main window
        update_gui_top()  # Start the update function for the Toplevel window
        
# Bind the function to the colored box's <Double-Button-1> event
canvas.bind('<Double-Button-1>', toggle_expand)


# Start the save function
window.after(2000, save_settings)

# Start the update function
window.after(10, update_gui)
start_monitoring()

# Function to stop the stream
def stop_stream(event=None):
    if 'stream' in globals():
        stream.stop()
        stream.close()

# Bind the function to the window's <Destroy> event
window.bind('<Destroy>', stop_stream)

# Start the Tkinter event loop
window.mainloop()
