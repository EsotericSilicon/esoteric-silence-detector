# -*- mode: python ; coding: utf-8 -*-


a = Analysis(
    ['SilenceAlarm.py'],
    pathex=[],
    binaries=[],
    datas=[('mic_128.ico', '.'), ('mic_128.png', '.')],
    hiddenimports=['SoundDevice', 'numpy'],
    hookspath=[],
    hooksconfig={},
    runtime_hooks=[],
    excludes=[],
    noarchive=False,
)
pyz = PYZ(a.pure)

exe = EXE(
    pyz,
    a.scripts,
    a.binaries,
    a.datas,
    [],
    name='EsotericSilenceDetector',
    debug=False,
    bootloader_ignore_signals=False,
    strip=False,
    upx=True,
    upx_exclude=[],
    runtime_tmpdir=None,
    console=False,
    disable_windowed_traceback=False,
    argv_emulation=False,
    target_arch=None,
    codesign_identity=None,
    entitlements_file=None,
    icon=['mic_128.ico'],
)
